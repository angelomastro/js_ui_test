window.addEventListener('load', function () {
	console.log("event listener");

	//initialise radio buttons from the servers
	createRadioButtons("services.json");

	function sendData()	{
  		console.log("sendData");

		let getPost = function(divName){
			let divRadio = document.getElementById(divName);
			let ret = null;
			for (let i = 0; i < divRadio.children.length; i++){
				let childRadio = divRadio.children[i];
				let childLabel = childRadio.children[1];
				let childInput = childRadio.children[0];
				if (childInput.checked === true){
					ret = {service: childLabel.innerText, 
						   id: childLabel.dataset.servernum};
				}
			}
			return ret;
		}
		let data = getPost("dynamic_radio");
		console.log("posting " + JSON.stringify(data));
		//posting the msg, asynchronously
		let XHR = new XMLHttpRequest();
		XHR.open('POST', 'server.php', true);
		XHR.setRequestHeader('Content-Type','multipart/form-data; boundary=blob');
		XHR.setRequestHeader('Content-Length', data.length);
		XHR.send(data);

	};
/*
	// POSTing the form with Async-Javascript
	let form = document.getElementById("form_id");
	form.addEventListener('submit', function (event){
			event.preventDefault();
			sendData();
		}
	);
*/

	let btn_individual = document.getElementById("btn_individual");
	btn_individual.addEventListener('click', function (event){
			console.log("event btn_individual");
			event.preventDefault();
			let first = document.getElementById("form_fistname");
				first.children[0].children[0].innerText = "First Name";
				first.children[1].children[0].setAttribute("placeholder","First Name");
			let last = document.getElementById("form_lastname");
				last.children[0].children[0].innerText = "Last Name";
				last.children[1].children[0].setAttribute("placeholder","Last Name");
			first.style.display = 'block';
			last.style.display = 'block';
			console.log("first name " + first.style.display);
			console.log("second name " + last.style.display);
		}
	);

	let btn_organisation = document.getElementById("btn_organisation");
	btn_organisation.addEventListener('click', function (event){
			event.preventDefault();
			console.log("event btn_organisation");
			let first = document.getElementById("form_fistname");
			let last = document.getElementById("form_lastname");
				last.children[0].children[0].innerText = "Name";
				last.children[1].children[0].setAttribute("placeholder","Name");
			first.style.display = 'none';
			last.style.display = 'block';
			console.log("first name " + first.style.display);
			console.log("second name " + last.style.display);
		}
	);

	let btn_anonymous = document.getElementById("btn_anonymous");
	btn_anonymous.addEventListener('click', function (event){
			event.preventDefault();
			console.log("event btn_anonymous");
			let first = document.getElementById("form_fistname");
			let last = document.getElementById("form_lastname");
			console.log("first name " + first.style);
			first.style.display = 'none';
			last.style.display = 'none';

			console.log("second name " + last.style.display);
		}
	);

})


function readServices(pathServices, callback){
	//reading the file/service, asynchronously 
	let rawFile = new XMLHttpRequest();
    rawFile.open("GET", pathServices, false);
	rawFile.onreadystatechange = function() {
		let DONE = this.DONE || 4;
		if(this.readyState == DONE) {
			let data = rawFile.responseText;
			let services = JSON.parse(data);
			callback(services);
		}
	}
	rawFile.send(null);
}

function createRadioButtons(pathServices){
	console.log("fill_radio_buttons from : " + pathServices);

	readServices(pathServices, function(services) {
		//posting the radio buttons on the page accordingly 
		let targetDiv = document.getElementById("dynamic_radio");
		for(let s in services){
			let sId = s; 
			let sName = services[s];
			let newInput = document.createElement("input");
				newInput.setAttribute("type","radio");
				newInput.setAttribute("name","optradio");
			let newLabel = document.createElement("label");
				newLabel.innerText = sName;
				newLabel.setAttribute("class","labradio");
				newLabel.dataset.servernum = sId;
			let newDiv = document.createElement("div");
				newDiv.setAttribute("class","radio");
				newDiv.appendChild(newInput);
				newDiv.appendChild(newLabel);
			targetDiv.appendChild(newDiv);
		}
	});
}

//only jQuery here
$(document).ready(function(){
	console.log("loading page with jQuery");

	let dataToPost = function(divName){
		let divRadio = document.getElementById(divName);
		let ret = null;
		for (let i = 0; i < divRadio.children.length; i++){
			let childRadio = divRadio.children[i];
			let childLabel = childRadio.children[1];
			let childInput = childRadio.children[0];
			if (childInput.checked === true){
				ret = {service: childLabel.innerText, 
					   id: childLabel.dataset.servernum};
			}
		}
		return ret;
	}
	
	//jQuery posting the form
    $("#form_submit").click(function(){		
		let data = dataToPost("dynamic_radio");
		alert("jQuery posting " + JSON.stringify(data));
        $.post("server.php", data);
    });

});


